import {Router} from '@angular/router';
import {Component} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';

@Component({
	selector: 'so-aside',
	templateUrl: './aside.component.html',
	styleUrls: ['./aside.component.scss']
})

export class AsideComponent {
	constructor(public authService: AuthService, public router: Router) {}
	
	makeLogout() {
		this.authService.doLogout();
		
		this.router.navigateByUrl('login');
	}
}
