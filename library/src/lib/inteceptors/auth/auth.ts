import {Inject} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {HttpInterceptor,HttpEvent,HttpHandler,HttpRequest,HttpHeaders} from '@angular/common/http';

export class AuthInterceptor implements HttpInterceptor {
	constructor(@Inject(AuthService) public authService) {}
	
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (req.url.includes('token')) {
			const headers = new HttpHeaders({
				Authorization: 'Basic ' + btoa(`trusted-app:secret`)
			});
			
			const uri = req.clone({headers});
			
			return next.handle(uri);
		} else {
			const headers = new HttpHeaders({
				Authorization: 'Bearer ' + this.authService.getAccessToken()
			});
			
			const uri = req.clone({headers});
			
			return next.handle(uri);
		}
		
		return next.handle(req);
	}
}