import {Action} from '@ngrx/store';

export enum ActionTypes {
	CreateTask = '[TasklistComponent] CreateTask',
	SetAllTasks = '[TasklistComponent] SetAllTasks',
	SetValidationOnTaskForm = '[NovoComponent] SetValidationOnTaskForm',
	UpdateActionsOnTaskForm = '[NovoComponent] UpdateActionsOnTaskForm',
	SetTaskBeingEdited = '[EditComponent] SetTaskBeingEdited',
	SetUserOnTaskBeingEdited = '[EditComponent] SetUserOnTaskBeingEdited',
}

export class CreateTask implements Action {
	readonly type = ActionTypes.CreateTask;
	
	constructor(public payload: { task: any }) {}
}

export class SetAllTasks implements Action {
	readonly type = ActionTypes.SetAllTasks;
	
	constructor(public payload: { tasks: any }) {}
}

export class SetValidationOnTaskForm implements Action {
	readonly type = ActionTypes.SetValidationOnTaskForm;
	
	constructor(public payload: { page: string, fieldName: string,  validationType: string, validationMessage: string }) {}
}

export class SetUserOnTaskBeingEdited implements Action {
	readonly type = ActionTypes.SetUserOnTaskBeingEdited;
	
	constructor(public payload: { user: any }) {}
}

export class UpdateActionsOnTaskForm implements Action {
	readonly type = ActionTypes.UpdateActionsOnTaskForm;
	
	constructor(public payload: { page: string, actionName: string,  actionValue: any }) {}
}

export class SetTaskBeingEdited implements Action {
	readonly type = ActionTypes.SetTaskBeingEdited;
	
	constructor(public payload: { fieldName: any, fieldValue: any }) {}
}

