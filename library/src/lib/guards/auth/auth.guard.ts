import {Router} from '@angular/router'
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

@Injectable({
	providedIn: 'root'
})

export class AuthGuard implements CanActivate {
	constructor(public authService: AuthService, public router: Router) {}
	
	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		const isValid = this.authService.isTokenValid();
		
		if (!isValid) {
			this.router.navigateByUrl('login');
		}
		
		return isValid;
	}
}
